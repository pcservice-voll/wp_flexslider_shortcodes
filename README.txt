=== WP Flexslider Shortcodes ===
Contributors: pcservice-voll
Donate link: http://media-store.net/wp-flexslider-shortcodes/
Tags: Flexslider, Gallary, Image-Slider, Images, Wordpress-Gallery, Shortcode
Requires at least: 4.0
Tested up to: 4.4.2
Stable tag: 4.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Ermöglicht das erstellen von Slider und Galerien von WooThemes(Flex Slider 2)
direkt als Shortcode-Eingabe ohne lange HTML-Struckturen zu editieren.

== Description ==

Das Plugin Wp Flexslider Shortcodes ermöglicht das nutzen des allgemein sehr beliebten und erfolgreichen OpenSource Sliders
FlexSlider2 von Woothemes in Wordpress Posts, Pages und Sidebars.
Dabei wird die Hauseigene Gallery von WordPress zur Auswahl der Bilder verwendet.

Dabei werden die einzelnen ID's der Bilder herausgefiltert und durch eine WordPress-Funktion der Link zum jeweiligen Bild gebildet.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `wp_flexslider_shortcodes.zip` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. Mode = Basic Slider
2. Mode = Slider with Custom Navigation
3. Mode = Slider with Caption
4. Mode = Slider with Thumbnails
5. Mode = Slider with Thumbnails Carousell
6. Admin Help Page

== Changelog ==

= 2.1.1 =

* Anpassungen an Wordpress 4.4.2

= 2.1.0 =

* Add to the Wordpress Plugins

== Upgrade Notice ==

* Anpassungen an Wordpress 4.4.2 vorgenommen.

== Arbitrary section ==
